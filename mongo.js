const { MongoClient, ObjectId } = require("mongodb");

exports.initializeDatabase = initializeDatabase;
exports.saveEncryptedGub = saveEncryptedGub;
exports.findEncryptedGub = findEncryptedGub;

const env = {
  url: process.env.DB_URL || "localhost/gubbins",
  creds:
    typeof process.env.DB_USERNAME !== "undefined"
      ? `${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@`
      : ""
};

function initializeDatabase() {
  return new Promise((resolve, reject) => {
    MongoClient.connect(`mongodb://${env.creds}${env.url}`, (err, db) => {
      if (err) {
        console.log(err);
        return reject(err);
      }
      resolve(db);
    });
  });
}

function saveEncryptedGub(db, encryptedGub, options = {}) {
  const expiration = options.lifespan
    ? new Date().getTime() + (options.lifespan * 3600 * 1000)
    : undefined;
  const obj = {
    encryptedGub,
    expiration,
    charges: options.charges,
    from: options.from,
    confirm: options.confirm
  };
  return new Promise((resolve, reject) => {
    db.collection("gubs").insertOne(obj, err => {
      if (err) {
        return reject(err);
      }
      resolve(obj._id);
    });
  });
}

function findEncryptedGub(db, id) {
  return new Promise((resolve, reject) => {
    db.collection("gubs").findOne({ _id: ObjectId(id) }, (err, gub) => {
      if (err) {
        return reject(err);
      }
      if(!gub || (gub.expiration && new Date(gub.expiration) < new Date())) {
        return resolve(undefined);
      }
      if (gub.charges === 1) {
        db.collection("gubs").remove({ _id: ObjectId(id) });
      } else {
        db
          .collection("gubs")
          .update({ _id: ObjectId(id) }, { $inc: { charges: -1 } });
      }
      resolve(gub);
    });
  });
}
