const { json, send } = require("micro");
const { router, get, post } = require("microrouter");
const cors = require("micro-cors")();

const { generateEncryptedGub, decryptGub } = require("./encryption");
const {
  initializeDatabase,
  saveEncryptedGub,
  findEncryptedGub
} = require("./mongo");

const db = initializeDatabase();

module.exports = cors(router(
  post("/gub", generateGub),
  get("/gub/:gub", generateGub),
  get("/gub/:id/key/:key", readGub)
));

async function generateGub(req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  try {
    const { gub, charges, from, lifespan, confirm } =
      req.method === "POST" ? await json(req) : req.params;
    const { key, encryptedGub } = await generateEncryptedGub(gub);
    const encodedKey = key.toString("hex");
    const id = await saveEncryptedGub(await db, encryptedGub, {
      charges: Number(charges || 1),
      from,
      lifespan: Number(lifespan),
      confirm: !!confirm
    });
    return send(res, 201, {
      url: `/gub/${id}/key/${encodedKey}`,
      id,
      key: encodedKey
    });
  } catch (e) {
    console.log(e);
    return send(res, 500, `<h1>There was an error processing your gubs</h1>`);
  }
}

async function readGub(req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  try {
    if (
      typeof req.params.id === "undefined" ||
      req.params.id.length !== 24 ||
      typeof req.params.key === "undefined"
    ) {
      return send(res, 400, `<h1>Please specify a valid gub ID and key</h1>`);
    }
    const gubRecord = await findEncryptedGub(await db, req.params.id);
    if (!gubRecord) {
      return send(res, 404, `<h1>No Gubs Found</h1>`);
    }
    return send(res, 200, {
      gub: decryptGub(gubRecord.encryptedGub, req.params.key),
      from: gubRecord.from,
      confirm: gubRecord.confirm
    });
  } catch (e) {
    console.log(e);
    return send(res, 500, `<h1>There was an error processing your gubs</h1>`);
  }
}
