import test from "ava";
import { generateEncryptionKey, encryptGub, decryptGub } from "./encryption";

test("generateEncryptionKey should return a 256 bit key and 16 bit IV", async t => {
  const { key, iv } = await generateEncryptionKey();

  t.is(key.toString("hex").length, 64);
  t.is(iv.toString("hex").length, 32);
});

test("generateEncryptionKey should different keys and IVs every time (100 iterations)", async t => {
  const numberOfIterations = 100;
  const iterations = await Promise.all(
    new Array(numberOfIterations).fill(true).map(() => generateEncryptionKey())
  );
  const keys = new Set(iterations.map(({ key }) => key));
  const IVs = new Set(iterations.map(({ iv }) => iv));

  t.true(iterations.length === numberOfIterations);
  t.true(iterations.length === keys.size);
  t.true(keys.size === IVs.size);
});

test("should be able to return some value from encryptGub", async t => {
  const { key, iv } = await generateEncryptionKey();
  const encryptedGub = encryptGub("some value", { key, iv });
  t.truthy(encryptGub);
});

test("should be able to decrypt an encrypted gub with the same key and iv", async t => {
  const { key, iv } = await generateEncryptionKey();
  const gub = "some value ohohoijoijoioijoij";
  const encryptedGub = encryptGub(gub, { key, iv });
  const decryptedGub = decryptGub(encryptedGub, key.toString("hex"));
  t.is(decryptedGub, gub);
});
