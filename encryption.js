const crypto = require("crypto");

exports.generateEncryptionKey = generateEncryptionKey;
exports.encryptGub = encryptGub;
exports.generateEncryptedGub = generateEncryptedGub;
exports.decryptGub = decryptGub;
exports.randomBytes = randomBytes;

function generateEncryptionKey() {
  return Promise.all([
    // hex for url safety, so the length is * 2 after casting to a string
    randomBytes(32),
    randomBytes(16)
  ]).then(arr => ({
    key: arr[0],
    iv: arr[1]
  }));
}

function encryptGub(gub, { key, iv }) {
  const cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  return `${iv.toString("hex")}_${Buffer.concat([
    cipher.update(gub),
    cipher.final()
  ]).toString("hex")}`;
}

async function generateEncryptedGub(gub) {
  const { iv, key } = await generateEncryptionKey();
  return {
    key,
    encryptedGub: encryptGub(gub, { key, iv })
  };
}

function decryptGub(gub, key) {
  // hex encoded string, there won't ever be an extra "_"
  const [iv, encryptedGub] = gub.split("_").map(i => Buffer.from(i, "hex"));
  const decipher = crypto.createDecipheriv(
    "aes-256-cbc",
    Buffer.from(key, "hex"),
    iv
  );
  return Buffer.concat([
    decipher.update(encryptedGub),
    decipher.final()
  ]).toString();
}

function randomBytes(length = 256) {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(length, (e, buffer) => {
      if (e) {
        reject(e);
        return;
      }
      resolve(buffer);
    });
  });
}
